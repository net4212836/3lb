﻿public interface ICalc
{
    int Sum(int x, int y);
    int Substract(int x, int y);
    int Multiply(int x, int y);
    double Divide(int x, int y);
}

