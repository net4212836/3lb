﻿public class Middleware
{
    private readonly RequestDelegate next;

    public Middleware(RequestDelegate next)
    {
        this.next = next;
    }

    public async Task InvokeAsync(HttpContext context, ICalc calcService)

    {

        context.Response.ContentType = "text/html;charset=utf-";
        Random rnd = new Random();
        int x = rnd.Next(0, 101);
        int y = rnd.Next(0, 101);

        await context.Response.WriteAsync($"<h>Result: {calcService.Divide(x, y)}</h>\n");
        await next.Invoke(context);

    }
}
