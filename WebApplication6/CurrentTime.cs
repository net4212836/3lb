﻿public class CurrentTime:ICurrentTime
{
    public string GetTime()
    {
        TimeSpan time = DateTime.Now.TimeOfDay;
        int hours = time.Hours;
        if (hours < 18 && hours > 12) return "Zaraz den";
        if (hours < 24 && hours > 18) return "Zaraz vechir";
        if (hours < 6 && hours > 0) return "Zaraz nich";
        else return "Zaraz ranok";
    }
}
