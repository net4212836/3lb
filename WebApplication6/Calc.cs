﻿public class CalcService: ICalc
{
    public int Sum(int x, int y) => x + y;
    public int Substract(int x, int y)=>x-y;
    public int Multiply(int x, int y) => x*y;
    public double Divide(int x, int y) { 
        if(y!=0) return x/y;
        throw new Exception("Division by zero is not allowed.");
    }
}

