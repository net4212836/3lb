var builder = WebApplication.CreateBuilder(args);
builder.Services.AddTransient<ICalc, CalcService>();
builder.Services.AddTransient<ICurrentTime, CurrentTime>();
var app = builder.Build();

app.UseMiddleware<Middleware>();
app.MapGet("/",async (context) =>
{
    var timeMessage = app.Services.GetService<ICurrentTime>();
    await context.Response.WriteAsync($"Time:{timeMessage?.GetTime()}");
});

app.Run();
